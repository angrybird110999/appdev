<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\OrderSectionController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\OwnerAuthController;
use App\Http\Controllers\UserAuthController;
use App\Http\Controllers\OwnerController;
use App\Http\Controllers\OwnerDashboardController;
use App\Http\Controllers\UserDashboardController;
use App\Http\Controllers\StoreController;
use App\Http\Controllers\StoreProductController;
use App\Http\Controllers\PaymentController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['middleware' => 'api', 'prefix' => 'auth'], function () {
    Route::group(['prefix' => 'owner'], function (){
        Route::post('login', [OwnerAuthController::class, 'login']);
        Route::post('store', [OwnerAuthController::class, 'store']);
        Route::post('logout', [OwnerAuthController::class, 'logout']);
        Route::post('me', [OwnerAuthController::class, 'me']);
        Route::put('update/{id}', [OwnerAuthController::class, 'update']);

    });
    Route::group(['prefix' => 'user'], function (){
        Route::post('login', [UserAuthController::class, 'login']);
        Route::post('store', [UserAuthController::class, 'store']);
        Route::post('logout', [UserAuthController::class, 'logout']);
        Route::post('me', [UserAuthController::class, 'me']);
        Route::put('update/{id}', [UserAuthController::class, 'update']);
    });
});

Route::group(['middleware' => 'api'], function (){

    Route::group(['prefix' => 'owner'], function (){
        Route::put('change_store_name/{id}', [OwnerController::class, 'updateStoreName']);

        Route::post('uploadFeaturedImage', [StoreController::class, 'uploadFeaturedImage']);
        Route::post('store', [StoreController::class, 'store']);
        Route::get('store', [StoreController::class, 'index']);
        Route::delete('store/destroy/{id}', [StoreController::class, 'deleteProduct']);
        Route::put('store/{id}', [StoreController::class, 'update']);

        Route::get('orders', [OrderSectionController::class, 'index']);
        Route::get('dashboard', [OwnerDashboardController::class, 'index']);
    });

    Route::group(['prefix' => 'user'], function (){
        Route::get('store', [StoreProductController::class, 'index']);
        Route::post('checkout', [PaymentController::class, 'checkout']);
        Route::get('dashboard', [userDashboardController::class, 'totalOrder']);
        Route::get('orders', [userDashboardController::class, 'orders']);
    });

});

Route::get('category', [CategoryController::class, 'index']);