<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Owner;
use App\Models\OwnerInfo;
use App\Models\Store;

class OwnerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:owner');
    }

    public function updateStoreName(Request $request, $id){
        $ownerinfo = OwnerInfo::where('id', auth('owner')->user()->id)->firstOrFail();
        $ownerinfo->update(['store_name' => $request->name]);
        return response()->json(['Store Name updated successfully!'], 200);
    }
}
