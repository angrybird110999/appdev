<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Store;
use Illuminate\Http\Request;

class OwnerDashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:owner');
    }

    public function index(){
        $prod = Store::where('owner_id', auth('owner')->user()->id)->count();
        $order = OrderItem::whereHas('store', function($query){
            $query->where('owner_id', auth('owner')->user()->id);
        })->count();
        return response()->json(['prod' => $prod, 'order' => $order ], 200);
    }
}
