<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\OrderInfo;
use App\Models\OrderItem;
use App\Models\Store;
use Exception;

use Stripe;

class PaymentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function checkout(Request $request)
    { 
        if($request->payment_type == 'Credit Card'){
            try {
                $stripe = new \Stripe\StripeClient(env('STRIPE_SECRET'));
                $token = $stripe->tokens->create([
                    'card' => [
                        'number' => $request->number,
                        'exp_month' => $request->exp_month,
                        'exp_year' => $request->exp_year,
                        'cvc' => $request->cvc,
                    ],
                ]);
        
                Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
                $response = Stripe\Charge::create ([
                        "amount" => $request->total * 100,
                        "currency" => "php",
                        "source" => $token,
                        "description" => "Food Order payment" 
                ]);
            }catch(Exception $e){
                return response()->json(['msg' => $e->getMessage()], 422);
            }

            $order = Order::create([
                'user_id' => auth('api')->user()->id,
                'total' => $request->total,
                'cash' =>  $request->total,
                'change' => 0,
                'payment_method' => $request->payment_type,
            ]);

            foreach($request->cart as $item){
                OrderItem::create([
                    'store_id' => $item['id'],
                    'order_id' => $order->id,
                    'qty' => $item['qty'],
                    'total' => $item['qty'] * $item['price'],
                ]);

                $store = Store::find($item['id']);
                if($store){
                    $store->update(['qty' => ($store->qty - $item['qty'])]);
                }
            }

            return response()->json(['msg' => 'Order created successfully'], 200);
        }
        else {

        Order::create([
            'user_id' => auth('api')->user()->id,
            'total' => $request->payment_fee,
            'cash' =>  $request->cash,
            'change' => $request->change,
            'payment_method' => $request->payment_method,
        ]);

            return response()->json(['msg' => 'Order created successfully'], 200);
        }
        return response()->json(['msg' => 'Success'], 200);
    }
}
