<?php

namespace App\Http\Controllers;

use App\Models\Store;
use App\Models\Owner;
use Illuminate\Http\Request;

class StoreController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth:owner');
    }

    public function index(){
        $items = Store::with(['category'])->where('owner_id', auth()->user()->id)->get();
        return response()->json($items, 200);
    }

    public function storeName(Request $request){
        $storename = Store::create([
            'name' => $request->store_name
        ]);

        if($storename){
            return response()->json(['name' => $storename], 200);
        } else {
            return response()->json(['msg' => 'Sorry, something went wrong!'], 404);
        }
    }

    public function store(Request $request){
      $data = [
        'image' => $request->image,
        'food_name' =>  $request->name,
        'category_id' => $request->category_id,
        'price' => $request->price,
        'qty' => $request->qty,
        'owner_id' => auth('owner')->user()->id
      ];

      Store::create($data);
      return response()->json(['msg' => 'Product saved successfully!'], 200);
    }

    public function update(Request $request, $id){
        $data = Store::where('id', $id)->first();
        $data->update([
            'image' => $request->image,
            'food_name' =>  $request->name,
            'category_id' => $request->category_id,
            'price' => $request->price,
            'qty' => $request->qty,
                    ]);
        return response()->json($data);
    }

    public function deleteFileFromServer($filename){
        $filePath = public_path().'/uploads/'.$filename;
        if(file_exists($filePath)){
            @unlink($filePath);
        }
        return;
    }

    public function uploadFeaturedImage(Request $request){
        $picName = time().'.'.$request->file->extension();
        $request->file->move(public_path('uploads'), $picName);
        return $picName;
    }

    public function deleteProduct($id){
        Store::destroy($id);
        return response()->json($id);
    }
}
