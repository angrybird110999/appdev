<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\UserAccount;
use Illuminate\Support\Facades\Hash;

class UserAccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserAccount::create([
            'email' => 'capalesedra@gmail.com',
            'password' => Hash::make('1234'),
            'account_status' => 'Activated',
        ]);

       
    }
}
