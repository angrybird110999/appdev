import API from '../base'

export default {
  namespaced: true,
  state: {
    summary: [],
    usersummary: [],
    orders: [],
  },
  mutations: {
    SET_SUMMARY(state, data) {
      state.summary = data
    },
    SET_USER_SUMMARY(state, data) {
      state.usersummary = data
    },
    SET_USER_ORDERS(state, data) {
      state.orders = data
    },
  },
  actions: {
    async getSummary({commit}){
      const res = await API.get('/owner/dashboard').then(res => {
        commit('SET_SUMMARY', res.data)
        return res;
      }).catch(err => {
        return err.response;
      })

      return res;
    },
    async getUserSummary({commit}){
      const res = await API.get('/user/dashboard').then(res => {
        commit('SET_USER_SUMMARY', res.data)
        return res;
      }).catch(err => {
        return err.response;
      })

      return res;
    },
    async getUserOrder({commit}){
      const res = await API.get('/user/orders').then(res => {
        commit('SET_USER_ORDERS', res.data)
        return res;
      }).catch(err => {
        return err.response;
      })

      return res;
    },
    
  }
}