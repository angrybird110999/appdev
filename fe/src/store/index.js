import Vue from 'vue'
import Vuex from 'vuex'

import auth from './auth'
import storename from './store'
import orders from './order'
import dashboard from './dashboard'

Vue.use(Vuex);

export default new Vuex.Store({
 modules: {
  auth, storename, orders, dashboard
 }
})

