import API from '../base/'

export default {
  namespaced: true,
  state: {
    user: [],
    useraccount: [],
    owneraccount: {
      ownerinfo: {
        first_name: '',
        last_name: '',
      }
    },
    token: localStorage.getItem('auth') || ''
  },
  mutations: {
    PUSH_NEW_USER(state, data){
      state.user.push(data)
    },
    SET_OWNER_ACC(state, data){
      state.owneraccount = data
      const bearer_token = localStorage.getItem('auth') || ''
      API.defaults.headers.common['Authorization'] = `Bearer ${bearer_token}`
    },
    SET_USER_ACC(state, data){
      state.useraccount = data
      const bearer_token = localStorage.getItem('auth') || ''
      API.defaults.headers.common['Authorization'] = `Bearer ${bearer_token}`
    },
    SET_AUTH_OWNER_TOKEN(state, token) {
     localStorage.setItem('auth', token)
     localStorage.setItem('isOwner', 'true')
     state.token = token

     const bearer_token = localStorage.getItem('auth') || ''
     API.defaults.headers.common['Authorization'] = `Bearer ${bearer_token}`
    },
    SET_AUTH_USER_TOKEN(state, token) {
     localStorage.setItem('auth', token)
     localStorage.setItem('isUser', 'true')
     state.token = token

     const bearer_token = localStorage.getItem('auth') || ''
     API.defaults.headers.common['Authorization'] = `Bearer ${bearer_token}`
    },
    UNSET_OWNER(state){
     localStorage.removeItem('auth');
     localStorage.removeItem('isOwner');
     state.token = ''
     state.user = ''

     API.defaults.headers.common['Authorization'] = ''
   }, 
    UNSET_USER(state){
     localStorage.removeItem('auth');
     localStorage.removeItem('isUser');
     state.token = ''
     state.user = ''

     API.defaults.headers.common['Authorization'] = ''
   } 
  },
  actions: {
    async loginOwnerAccount({commit}, payload){
      const res = await API.post('/auth/owner/login', payload).then(res => {
        commit('SET_OWNER_ACC', res.data)
        commit('SET_AUTH_OWNER_TOKEN', res.data.access_token)

        return res;
      }).catch(err => {
       return err
      })

      return res;
    },
    async logoutOwner({commit}){
     const res = await API.post('auth/owner/logout?token=' + localStorage.getItem('auth')).then(response => {
       commit('UNSET_OWNER')
       return response
     }).catch(error => {
       return error.response
     });

     return res;
    },
    async updateAdminAccount({commit},payload){
      const res = await API.put(`/auth/owner/update/${payload.id}`, payload).then(res => {
        return res;
      }).catch(err => {
       return err.response;
      })

      return res;
    },
    async updateUserAccount({commit},payload){
      const res = await API.put(`/auth/user/update/${payload.id}`, payload).then(res => {
        return res;
      }).catch(err => {
       return err.response;
      })

      return res;
    },
    async loginUserAccount({commit}, payload){
      const res = await API.post('/auth/user/login', payload).then(res => {
        commit('SET_USER_ACC', res.data)
        commit('SET_AUTH_USER_TOKEN', res.data.access_token)

        return res;
      }).catch(err => {
       return err
      })

      return res;
    },
    async logoutUser({commit}){
     const res = await API.post('auth/user/logout?token=' + localStorage.getItem('auth')).then(response => {
       commit('UNSET_USER')
       return response
     }).catch(error => {
       return error.response
     });

     return res;
    },
    async createOwnerAccount({commit}, payload){
      const res = await API.post('/auth/owner/store', payload).then(res => {
        commit('PUSH_NEW_USER', payload)
        return res;
      }).catch(err => {
      return err.response;
      })

      return res;
    },
    async createUserAccount({commit}, payload){
      const res = await API.post('/auth/user/store', payload).then(res => {
        commit('PUSH_NEW_USER', payload)
        return res;
      }).catch(err => {
      return err.response;
      })

      return res;
    },
    async checkAdminUser({commit}) {
      const res = await API.post('auth/owner/me?token=' + localStorage.getItem('auth')).then(res => {
        commit('SET_OWNER_ACC', res.data)
        return res;
      }).catch(error => {
        return error.response;
      })
  
      return res;
     },
     async checkAuthUser({commit}) {
      const res = await API.post('auth/user/me?token=' + localStorage.getItem('auth')).then(res => {
        commit('SET_USER_ACC', res.data)
  
        return res;
      }).catch(error => {
        return error.response;
      })
  
      return res;
     },
  },
}