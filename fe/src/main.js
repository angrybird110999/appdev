import Vue from 'vue'
import App from './App.vue'
import VueFileAgent from 'vue-file-agent';
import VueCarousel from 'vue-carousel';
import {BootstrapVue, IconsPlugin} from 'bootstrap-vue'
import Toast from "vue-toastification";
import router from './routes'
import store from './store'

Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.use(VueCarousel);

Vue.use(VueFileAgent);
import 'bootstrap'
import './assets/css/style.css'
import 'bootstrap/dist/css/bootstrap.css'
import "vue-toastification/dist/index.css";
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'vue-file-agent/dist/vue-file-agent.css';
import '@fortawesome/fontawesome-free/js/all.js'
import '@fortawesome/fontawesome-free/css/all.css'

Vue.use(Toast, {
  maxToasts: 4,
  newestOnTop: true,
  position: "top-right",
  hideProgressBar: true,
  pauseOnHover: true
});

Vue.config.productionTip = false



new Vue({
  router, store,
  render: h => h(App)
}).$mount('#app')
